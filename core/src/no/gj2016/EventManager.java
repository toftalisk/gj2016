package no.gj2016;

import com.badlogic.gdx.math.Vector2;
import no.gj2016.events.AreaDamage;
import no.gj2016.gameObjects.SimpleSpriteObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import no.gj2016.gameObjects.SimpleSpriteObject;

import java.util.List;
/**
 * Created by are on 30.01.16.
 */
public class EventManager {
    private static final float LINE_WIDTH = 0.2f;

    private Set<String> stateFlags;
    private int winState;
    private int babySouls;

    public ArrayList<AreaDamage> pendingAreaDamage;
    
    private float timeSlow;
    
    private Vector2 skull_direction;
    private Vector2 skull_position;
    private boolean newSkull = false;

    public EventManager() {
    reset();
    }

    void reset() {

        this.stateFlags = new HashSet<String>();
        this.winState = 0;
        this.pendingAreaDamage = new ArrayList<AreaDamage>();
        this.babySouls = 0;
    }

    public void increaseBabySouls() {
        babySouls++;
    }

    public void decreaseBabySouls() {
        babySouls--;
    }

    public int getBabySouls() {
        return babySouls;
    }

    public void increaseWinState()
    {
        this.winState ++;
    }

    public int getWinState()
    {
        return this.winState;
    }

    public void applyAreaDamage(AreaDamage areaDamage)
    {
        pendingAreaDamage.add(areaDamage);
    }

    public void setFlag(String flag)
    {
        System.out.println("FLAG SET = " + flag);
        this.stateFlags.add(flag);
    }

    public boolean checkFlag(String flag)
    {
        return this.stateFlags.contains(flag);
    }

    public void resolveDamage(ArrayList<SimpleSpriteObject> objects)
    {
        for (AreaDamage dmg : pendingAreaDamage) {
            for (SimpleSpriteObject so : objects) {
                if (dmg.shouldDamage(new Vector2(so.getX(), so.getY()), so.getType()))
                {
                    so.takeDamage(dmg.getDamage(), this);
                }
            }
        }

        pendingAreaDamage.clear();
    }

    public void setTimeSlow(float timeSlow) {
        this.timeSlow = timeSlow;
    }

    public float getSlowTime() {
        return timeSlow;
    }

    public void setWinState(int i) {
        winState = i;
    }

    public void setBabySouls(int babies) {
        babySouls = babies;
    }
    
    public void addSkull(Vector2 startVector, Vector2 direction_vector)
    {
    	skull_direction = direction_vector;
    	skull_position = startVector;
    	newSkull = true;
    }
    
    public boolean istNewSkull()
    {    	
    	return newSkull;
    }
    
    public Vector2 getSkullDirection()
    {
    	return skull_direction;
    }
    public Vector2 getSkullPosition()
    {
		return skull_position;
    }
    public void clearSKull()
    {
    	newSkull = false;
    }
}
