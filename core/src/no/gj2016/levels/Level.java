package no.gj2016.levels;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.sun.media.sound.SimpleSoundbank;

import no.gj2016.EventManager;
import no.gj2016.events.SeekInfo;
import no.gj2016.events.SeekRaycastCallback;
import no.gj2016.gameObjects.*;
import no.gj2016.sound.SoundHandler;
import no.gj2016.utils.SaneCamera;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.List;
/**
 * Created by are on 31.01.16.
 */
public class Level {
    TiledMap tiledMap;
    TiledMapRenderer tiledMapRenderer;
    float tileWidth;

    public static Level loadedLevel = null;
    public static World physicsWorld = new World(new Vector2(0.0f, 0.0f), true);

    SoundHandler soundHandler;

    Set<Body> physicBodiesToCleanUp;
    ArrayList<SimpleSpriteObject> spriteObjects;
    EventManager eventManager;

    Level nextLevel;

    public Level(String tileMapFilename, SoundHandler soundHandler)
    {
        tiledMap = new TmxMapLoader().load(tileMapFilename);
        tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);

        tileWidth = tiledMap.getProperties().get("tilewidth", Integer.class);

        physicBodiesToCleanUp = new HashSet<Body>();

        eventManager = new EventManager();
        spriteObjects = new ArrayList<SimpleSpriteObject>();

        this.soundHandler = soundHandler;
    }

    public void setNextLevel(Level nextLevel)
    {
        this.nextLevel = nextLevel;
    }

    public Level getNextLevel()
    {
        return nextLevel;
    }

    public boolean haveWon()
    {
        return eventManager.getWinState() >= 4;
    }

    public boolean haveLost() {
        int c = 0;
        for (Monk m: getMonks()) {
            if (m.alive()) {
                c++;
            }
        }
         return c <=3;
    }

    public void load()
    {
        if (loadedLevel != null)
        {
            loadedLevel.unload();
        }

        createTileCollisionBodies();

        createGameObjects();

        setupCollisions();

        loadedLevel = this;
    }

    public void unload()
    {
        for (Body b : physicBodiesToCleanUp) {
            physicsWorld.destroyBody(b);
        }

        spriteObjects.clear();
    }

    private Body createSquareBody(Vector2 center, Vector2 size)
    {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(center.x, center.y);
        bodyDef.type = BodyDef.BodyType.KinematicBody;

        Body body = physicsWorld.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(size.x / 2.0f, size.y / 2.0f);

        FixtureDef fixtureDef = new FixtureDef();

        fixtureDef.shape = shape;
        fixtureDef.density = 0.0f;
        fixtureDef.friction = 0.4f;
        fixtureDef.restitution = 0.0f;

        body.createFixture(fixtureDef);

        shape.dispose();

        return body;
    }


    private void createTileCollisionBodies()
    {
        Vector2 boxCenter = new Vector2();

        for (MapLayer layer : tiledMap.getLayers()) {
            if (layer instanceof TiledMapTileLayer) {
                TiledMapTileLayer tiles = (TiledMapTileLayer)layer;

                for (int i = 0; i<tiles.getWidth(); i++) {
                    for (int j = 0; j<tiles.getHeight(); j++) {
                        TiledMapTileLayer.Cell c = tiles.getCell(i, j);
                        if (c.getTile().getProperties().containsKey("wall")){
                            Vector2 position = new Vector2(i + 0.5f, j + 0.5f);
                            Body body = createSquareBody(position, new Vector2(1.0f, 1.0f));
                            physicBodiesToCleanUp.add(body);
                            GameObject wall = new Wall(position.x, position.y);
                            body.setUserData(wall);
                        }
                    }
                }
            }
        }

        /// loading the borders I think?
        MapObjects collides = tiledMap.getLayers().get("collide").getObjects();
        for (MapObject obj : collides) {

            float x = obj.getProperties().get("x", float.class)/tileWidth;
            float y = obj.getProperties().get("y", float.class)/tileWidth;

            float width = obj.getProperties().get("width", float.class)/tileWidth;
            float height = obj.getProperties().get("height", float.class)/tileWidth;

            Vector2 pos = new Vector2(x, y);
            Vector2 size = new Vector2(width, height);

            pos.add(new Vector2(size).scl(0.5f));

            physicBodiesToCleanUp.add(createSquareBody(pos, size));
        }
    }

    private void createGameObjects()
    {
        MapObjects objects = tiledMap.getLayers().get("obj").getObjects();
        // import objects
        for (MapObject obj : objects) {
            SimpleSpriteObject o;

            String type = obj.getProperties().get("type", String.class);
            if (type.equalsIgnoreCase("player")) {
                o = new Monk(obj.getName(), soundHandler);
            } else if (type.equalsIgnoreCase("Knight"))
            {
                o = new Knight("2d/" + obj.getName() + ".png", soundHandler);
            } else if (type.equalsIgnoreCase("baby"))
            {
                o = new Baby("2d/" + obj.getName() + ".png", soundHandler);
            } else if(type.equalsIgnoreCase("farmer")) {
                o = new Farmer(obj.getName(), soundHandler);
            }
            else {
                o = new SimpleSpriteObject("2d/" + obj.getName() + ".png", 1.4f, soundHandler);
            }


            float x = obj.getProperties().get("x", float.class);
            float y = obj.getProperties().get("y", float.class);

            float width = obj.getProperties().get("width", float.class);
            float height = obj.getProperties().get("height", float.class);

            x += width / 2.0f;
            y += height * (1.5f);

            addPhysicsAndStuff(o, type, x, y);
        }

    }

	private void addPhysicsAndStuff(SimpleSpriteObject o, String type, float x, float y) {
		BodyDef bodyDef =new BodyDef();
		bodyDef.linearDamping = 100;
		if (type.equalsIgnoreCase("door")) {
		    bodyDef.type = BodyDef.BodyType.StaticBody;
		} else {
		    bodyDef.type = BodyDef.BodyType.DynamicBody;
		}

		bodyDef.position.set(x/tileWidth, y/tileWidth);
		Body body = physicsWorld.createBody(bodyDef);
		physicBodiesToCleanUp.add(body);

		body.setUserData(o);

		CircleShape circle = new CircleShape();
		circle.setRadius(0.3f);

		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = circle;
		fixtureDef.density = 500f;
		fixtureDef.friction = 0.4f;
		fixtureDef.restitution = 0.6f;

		body.createFixture(fixtureDef);
		body.setFixedRotation(true);
		circle.dispose();

		o.setType(type);
		o.setBody(body);
		spriteObjects.add(o);
	}

    public EventManager getEventManager()
    {
        return eventManager;
    }

    public ArrayList<SimpleSpriteObject> getSpriteObjects() {
        return spriteObjects;
    }

    private void setupCollisions() {
        physicsWorld.setContactListener(new ContactListener() {
            @Override
            public void beginContact(Contact contact) {
                Object oa = contact.getFixtureA().getBody().getUserData();
                Object ob = contact.getFixtureB().getBody().getUserData();
                if (oa != null && oa instanceof GameObject &&
                        ob != null && ob instanceof GameObject) {

                    GameObject a = (GameObject)oa;
                    GameObject b = (GameObject)ob;

                    a.collidesWith(b, getEventManager());
                    b.collidesWith(a, getEventManager());
                }
            }

            @Override
            public void endContact(Contact contact) {

            }

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {

            }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {

            }
        });
    }

    public ArrayList<Vector2> getMonkPoints()
    {
        ArrayList<Vector2> out = new ArrayList<Vector2>();
        for (SimpleSpriteObject s : getSpriteObjects())
        {
            if (s.getType().equalsIgnoreCase("player"))
            {
                out.add(new Vector2(s.getX(), s.getY()));
            }
        }

        return out;
    }

    public void render(SaneCamera camera)
    {
        tiledMapRenderer.setView(camera.getTilemapCamera(tileWidth));
        tiledMapRenderer.render();
    }

    public boolean checkLineOfSight(GameObject from, GameObject to)
    {
        SeekRaycastCallback callback = new SeekRaycastCallback(to);
        physicsWorld.rayCast(callback, from.getPosition(), to.getPosition());

        return callback.wasSuccessful();
    }



    public void update(float dt)
    {
        physicsWorld.step(1/60f, 6, 2);

        for (GameObject so : spriteObjects)
        {
            if (so.seekEnabled()) {
                for (GameObject other : spriteObjects) {
                    SeekInfo seekInfo = so.getSeekInfo(other.getType());

                    if (seekInfo != null)
                    {
                        if (seekInfo.getRadius() > (so.getDistance(other))) {
                            if (!seekInfo.useLineOfSight() || checkLineOfSight(so, other)) {
                                so.onSeekFound(other);
                            }
                        }
                    }
                }
            }
        }
        
        
		if (eventManager.istNewSkull())
        {
			createSkull();
        }        
        
    }

	private void createSkull() {

		
		SimpleSpriteObject skull = new SimpleSpriteObject("2d/skull.png", 1.0f, SoundHandler.getInstance());
		
		float x = eventManager.getSkullPosition().x;
		float y = eventManager.getSkullPosition().y;
		
		addPhysicsAndStuff(skull, "skull", x, y);
		skull.setPos(x, y);
		
		eventManager.clearSKull();
		
		skull.setContinuousMove(eventManager.getSkullDirection());
	}

    public void removePendingObjects()
    {
        for (Iterator<SimpleSpriteObject> i = spriteObjects.iterator(); i.hasNext();)
        {
            SimpleSpriteObject o = i.next();

            if (o.shouldRemove() && !o.isRemoved())
            {
                if (!(o instanceof Monk)) {
                    physicBodiesToCleanUp.remove(o.getBody());
                    physicsWorld.destroyBody(o.getBody());

                    i.remove();
                } else {
                    o.setInactive();
                }
            }
        }
    }

    public ArrayList<Monk> getMonks()
    {
        ArrayList<Monk>  monks = new ArrayList<Monk>();

        for (SimpleSpriteObject so : spriteObjects)
        {
            if (so.getType().equals("player"))
            {
                monks.add((Monk)so);
            }
        }

        return monks;
    }
}
