package no.gj2016.sound;

/**
 * Created by maasbak on 30.01.2016.
 */
public enum GameSound {
    // Explosions
    EXPLOSION01, EXPLOSION02, EXPLOSION03, EXPLOSION04,
    // Bangs
    BANG01, BANG02, BANG03, BANG04,
    // Character sounds
    BABY, AUCH,
    // Weapon sounds
    SWORD,
}
