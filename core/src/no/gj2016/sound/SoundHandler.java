package no.gj2016.sound;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by maasbak on 30.01.2016.
 */
public class SoundHandler {
    
	private static SoundHandler instance = null;
	
	public static SoundHandler getInstance()
	{
		if (instance == null)
		{
			instance = new SoundHandler();
		}
		
		return instance;
	}
	
	
	
	public static HashMap<GameSound, Sound> soundMap;   
    
    private Random rand;

    public SoundHandler() {
        soundMap = new HashMap<GameSound, Sound>();
        rand = new Random();
        // Add explosions
        soundMap.put(GameSound.EXPLOSION01, Gdx.audio.newSound(Gdx.files.internal("sound/explosion-001.mp3")));
        soundMap.put(GameSound.EXPLOSION02, Gdx.audio.newSound(Gdx.files.internal("sound/explosion-002.mp3")));
        soundMap.put(GameSound.EXPLOSION03, Gdx.audio.newSound(Gdx.files.internal("sound/explosion-003.mp3")));
        soundMap.put(GameSound.EXPLOSION04, Gdx.audio.newSound(Gdx.files.internal("sound/explosion-004.mp3")));
        // Add bangs
        soundMap.put(GameSound.EXPLOSION01, Gdx.audio.newSound(Gdx.files.internal("sound/bang-001.mp3")));
        soundMap.put(GameSound.EXPLOSION02, Gdx.audio.newSound(Gdx.files.internal("sound/bang-002.mp3")));
        soundMap.put(GameSound.EXPLOSION03, Gdx.audio.newSound(Gdx.files.internal("sound/bang-003.mp3")));
        soundMap.put(GameSound.EXPLOSION04, Gdx.audio.newSound(Gdx.files.internal("sound/bang-004.mp3")));

        soundMap.put(GameSound.AUCH, Gdx.audio.newSound(Gdx.files.internal("sound/auch.mp3")));
        soundMap.put(GameSound.BABY, Gdx.audio.newSound(Gdx.files.internal("sound/phillip.ogg")));
    }

    public void PlaySound(GameSound sound) {
        PlaySound(sound, 100.0f);
    }

    public void PlaySound(GameSound sound, float volume) {
        Sound s = soundMap.get(sound);
        s.play(volume);
    }

    public void PlayBaby() {
        PlaySound(GameSound.BABY);
    }

    public void PlayAuch() {
        PlaySound(GameSound.AUCH);
    }

    public void PlayRandomExplosion() {
        // TODO: Fix cruft
        ArrayList<GameSound> explosions = new ArrayList<GameSound>();
        explosions.add(GameSound.EXPLOSION01);
        explosions.add(GameSound.EXPLOSION02);
        explosions.add(GameSound.EXPLOSION03);
        explosions.add(GameSound.EXPLOSION04);
        int choice = rand.nextInt(explosions.size());
        PlaySound(explosions.get(choice));
    }

    public void PlayRandomBang() {
        // TODO: Fix cruft
        ArrayList<GameSound> bangs = new ArrayList<GameSound>();
        bangs.add(GameSound.BANG01);
        bangs.add(GameSound.BANG02);
        bangs.add(GameSound.BANG03);
        bangs.add(GameSound.BANG04);
        int choice = rand.nextInt(bangs.size());
        PlaySound(bangs.get(choice));
    }
}
