package no.gj2016;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import no.gj2016.events.RectangleDamage;
import no.gj2016.particles.ParticleSquirter;
import no.gj2016.particles.RitualSpark;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mto on 30/01/2016.
 */
public class Square extends Ritual{
	
	ParticleSquirter particle = null;
	
	
    @Override
    public List<Vector2> getPoints() {
        List<Vector2> points = new ArrayList<Vector2>();
        points.add(new Vector2(0.0f, 0.0f));
        points.add(new Vector2(0.0f, 1.0f));
        points.add(new Vector2(1.0f, 0.0f));
        points.add(new Vector2(1.0f, 1.0f));

        return points;
    }

    @Override
    public int getPrimaryIndex() {
        return 0;
    }

    @Override
    public int getSecondaryIndex() {
        return 3;
    }

    @Override
    public void drawMe(SpriteBatch batch, ShapeRenderer shapeRenderer) {
        if (strength == 0.f) return;
        
        List<Line> lines = new ArrayList<Line>();
        //bottom
        lines.add(new Line(playerPoints.get(0), playerPoints.get(1)));
        //right
        lines.add(new Line(playerPoints.get(1), playerPoints.get(3)));
        //top
        lines.add(new Line(playerPoints.get(3), playerPoints.get(2)));
        //left
        lines.add(new Line(playerPoints.get(2), playerPoints.get(0)));

        RitualSpark.spawnBeam(lines, strength, 0.3f, 0.0f, 0.0f);

        
        if (particle != null)
        {
			batch.begin();
        	particle.draw(batch);
			batch.end();
        } 

             
        
    }

	@Override
	void update(float dt, EventManager eventManager) {
		if (strength == 0.f) {
			particle = null;
			return;
		}	
		
		if (strength >= 0.4f)
		{

			Vector2 center = new Vector2(0.0f, 0.0f);
			
			for (Vector2 point: playerPoints)
			{
				center.add(point);
			}
			
			center.scl(1.0f/playerPoints.size());
			
			
			
			if (particle == null)
			{
				particle = new ParticleSquirter("pentagram.particle");			
				particle.repeatable = true;
				particle.squirtAt(center.x, center.y);
			}
			else
			{
				particle.squirtAt(center.x, center.y);
			}

			particle.update(dt);

			float damageFactor = 7;
			RectangleDamage damage = new RectangleDamage(getDamageRectangle(), damageFactor * (damageFactor * strength) * (damageFactor * strength) * dt);
			damage.addImmune("player");
			damage.addImmune("door");
			eventManager.applyAreaDamage(damage);
		}
	}



	private Rectangle getDamageRectangle() {
		float left = Float.MAX_VALUE;
		float right = Float.MIN_VALUE;
		float top = Float.MIN_VALUE;
		float bottom = Float.MAX_VALUE;

		for (Vector2 player: playerPoints ) {
			if (player.x < left) left = player.x;
			if (player.x > right) right = player.x;
			if (player.y < bottom) bottom = player.y;
			if (player.y > top) top = player.y;
		}

		return new Rectangle(left, bottom, right - left, top - bottom);
	}
}
