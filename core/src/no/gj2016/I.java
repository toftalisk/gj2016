package no.gj2016;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import no.gj2016.particles.RitualSpark;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mto on 30/01/2016.
 */
public class I extends Ritual{
    @Override
    public List<Vector2> getPoints() {
        List<Vector2> points = new ArrayList<Vector2>();
        points.add(new Vector2(0.0f, 0.0f));
        points.add(new Vector2(0.0f, 1.0f));
        points.add(new Vector2(0.0f, 2.0f));
        points.add(new Vector2(0.0f, 3.0f));

        return points;
    }

    @Override
    public int getPrimaryIndex() {
        return 0;
    }

    @Override
    public int getSecondaryIndex() {
        return 3;
    }

    @Override
    public void drawMe(SpriteBatch batch, ShapeRenderer shapeRenderer) {
        if (strength == 0.f) return;
        List<Line> lines = new ArrayList<Line>();

        lines.add(new Line(playerPoints.get(0), playerPoints.get(1)));
        lines.add(new Line(playerPoints.get(1), playerPoints.get(2)));
        lines.add(new Line(playerPoints.get(2), playerPoints.get(3)));

        RitualSpark.spawnBeam(lines, strength, 1.0f, 0.0f, 1.0f);
    }

	@Override
	void update(float dt, EventManager eventManager) {
		// TODO Auto-generated method stub
		
	}
}
