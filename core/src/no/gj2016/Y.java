package no.gj2016;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Vector2;

import no.gj2016.gameObjects.SimpleSpriteObject;
import no.gj2016.particles.RitualSpark;
import no.gj2016.particles.SparkRenderer;
import no.gj2016.sound.SoundHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by are on 31.01.16.
 */
public class Y extends Ritual {
    private static float charge;
    Y()
    {
        super();

        charge = 0.0f;
    }
    @Override
    List<Vector2> getPoints() {
        List<Vector2> points = new ArrayList<Vector2>();
        points.add(new Vector2(-0.25f, 0.0f));
        points.add(new Vector2(+0.25f, 0.0f));
        points.add(new Vector2( 0.00f, 0.0f));
        points.add(new Vector2( 0.00f, 1.0f));

        return points;
    }

    @Override
    int getPrimaryIndex() {
        return 2;
    }

    @Override
    int getSecondaryIndex() {
        return 3;
    }

    @Override
    void drawMe(SpriteBatch batch, ShapeRenderer shapeRenderer) {
    }


    @Override
    void update(float dt, EventManager eventManager) {
        if (getStrength() == 0.0f)
        {
            charge -= dt;
        } else {
            charge += getStrength() * dt * 0.2f;
            if (charge > 1.0f)
            {
                Vector2 direction_vector = playerPoints.get(3).sub(playerPoints.get(2));
                Vector2 startVector = playerPoints.get(3);
                                
                eventManager.addSkull(startVector, direction_vector);
                
                
                charge = 0.0f;
            }
        }

        if (charge < 0.0f)
        {
            charge = 0.0f;
            return;
        }
        ArrayList<Line> list = new ArrayList<Line>();
        list.add(new Line(playerPoints.get(0), playerPoints.get(2)));
        list.add(new Line(playerPoints.get(1), playerPoints.get(2)));
        list.add(new Line(playerPoints.get(3), playerPoints.get(2)));

        RitualSpark.spawnBeam(list, charge, 1.0f, 0.0f, 0.0f);
    }
}
