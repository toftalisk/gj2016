package no.gj2016;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import no.gj2016.gameObjects.Monk;
import no.gj2016.gameObjects.PlayerCharacter;
import no.gj2016.gameObjects.SimpleSpriteObject;
import no.gj2016.levels.Level;
import no.gj2016.particles.ParticleSquirterSingleton;
import no.gj2016.particles.SparkRenderer;
import no.gj2016.sound.SoundHandler;
import no.gj2016.userInput.GamePad;
import no.gj2016.userInput.Keyboard;
import no.gj2016.userInput.UserInput;
import no.gj2016.utils.SaneCamera;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

public class UninterestedSquare extends ApplicationAdapter {
	Music backgroundMusic;
	SpriteBatch batch;


	SaneCamera camera;
	private List<Ritual> rituals;
    private ShapeRenderer shapeRenderer;

	private static float tileWidth = 150.0f;
	private static final float screenWidthInMonks = 32.0f;

	private SoundHandler soundHandler;
    private RitualVerifyer ritualVerifyer;

	private Level currentLevel;
	private Level start;

	@Override
	public void create() {
		// Load epic background music
		backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("sound/OppressiveGloom.mp3"));
		backgroundMusic.setLooping(true);
		backgroundMusic.setVolume(60.0f);
		backgroundMusic.play();

		// Prepare rituals
        this.ritualVerifyer = new RitualVerifyer();
		this.rituals = new ArrayList<Ritual>();
		this.rituals.add(new I());
		this.rituals.add(new L());
		this.rituals.add(new Square());
        this.rituals.add(new Hourglass());
		this.rituals.add(new Y());


		batch = new SpriteBatch();
        shapeRenderer = new ShapeRenderer();
        soundHandler = SoundHandler.getInstance();

		camera = new SaneCamera(screenWidthInMonks);
		camera.translate(16, 9);

		Level level1 = new Level("tiles/castle.tmx", soundHandler);
		Level level2 = new Level("tiles/castle2.tmx", soundHandler);
        Level level3 = new Level("tiles/castle3.tmx", soundHandler);
		level1.setNextLevel(level2);
        level2.setNextLevel(level3);

		start = level1;

		currentLevel = start;
		currentLevel.load();

		configureUserInputs(currentLevel.getMonks());
	}


	boolean babyAlive() {
		for (SimpleSpriteObject o : currentLevel.getSpriteObjects()) {
			if (o.getType().equalsIgnoreCase("baby") ) {
				return o.alive();
			}
		}
		return true;
	}


	private void configureUserInputs(AbstractList<Monk> players) {
		AbstractList<UserInput> gamePads = gamePads = new ArrayList<UserInput>();
        for(Controller controller : Controllers.getControllers())
        {
            Gdx.app.log("UserInput", "Added user input: " + controller.getName());
            GamePad gamePad = new GamePad();
            controller.addListener(gamePad);
            gamePads.add(gamePad);
        }

		for(PlayerCharacter player : players) {
			if(!gamePads.isEmpty()) {
				player.set(gamePads.get(0));
				gamePads.remove(0);
			}
            else
            {
                player.set(Keyboard.createKeyboard());
            }
		}
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		float aspect = ((float)width/(float)height);

		camera.setAspect(aspect);
	}

	void checkRitual() {
		List<Vector2> monk_pos = currentLevel.getMonkPoints();
        for (Ritual ritual: this.rituals) {
			if (ritual.getPoints().size() == monk_pos.size()) {
				ritualVerifyer.updateRitual(ritual, monk_pos);
			}
        }
	}

	void update(float dt) {
		currentLevel.update(dt);

		for (SimpleSpriteObject s : currentLevel.getSpriteObjects())
		{
			s.update(dt, currentLevel.getEventManager());
		}

        for (Ritual ritual: rituals) {
            ritual.update(dt, currentLevel.getEventManager());
        }

		ParticleSquirterSingleton.update(dt);

        checkRitual();

		SparkRenderer.getInstance().update(dt);

		currentLevel.getEventManager().resolveDamage(currentLevel.getSpriteObjects());

		currentLevel.removePendingObjects();

		if (currentLevel.haveWon())
		{
			Keyboard.releaseKeyboards();
			int babies = currentLevel.getEventManager().getBabySouls();
			currentLevel = currentLevel.getNextLevel();
			currentLevel.getEventManager().reset();
			currentLevel.getEventManager().setBabySouls(babies);
			loadLevel();

		}
		else if (currentLevel.haveLost()) {

			if (currentLevel.getEventManager().getBabySouls() > 0) {
				currentLevel.getEventManager().decreaseBabySouls();
				int win = currentLevel.getEventManager().getWinState();
				currentLevel.getEventManager().reset();
				currentLevel.getEventManager().setWinState(win);
				loadLevel();
			} else {
				currentLevel = start;
				currentLevel.getEventManager().reset();
				loadLevel();
			}
		}
	}

	void loadLevel() {
		Keyboard.releaseKeyboards();
		currentLevel.load();
		configureUserInputs(currentLevel.getMonks());
	}



	@Override
	public void render () {


		float dt = Gdx.graphics.getDeltaTime();

		update(dt);

		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		currentLevel.render(camera);

		batch.setProjectionMatrix(camera.getProjectionMatrix());
		batch.setTransformMatrix(camera.getTransformMatrix());

        //Shape renderer
        shapeRenderer.setProjectionMatrix(camera.getProjectionMatrix());
        shapeRenderer.setTransformMatrix(camera.getTransformMatrix());


		for (SimpleSpriteObject m : currentLevel.getSpriteObjects())
		{
			m.drawMe(batch, shapeRenderer);
		}

        for (Ritual ritual: rituals) {
			ritual.drawMe(batch, shapeRenderer);
		}

		ParticleSquirterSingleton.drawAll(batch);
		
		SparkRenderer.getInstance().render(shapeRenderer);


	}
}
