package no.gj2016.utils;

import com.badlogic.gdx.math.Vector2;
import junit.framework.TestCase;

public class AngleToolTest extends TestCase {

    public void testDirectionAngle()
    {
        float angle = 10.0f;

        Vector2 r = AngleTool.rotateVectorDeg(new Vector2(0.0f, 0.05f), angle);

        float angle2 = AngleTool.angleFromVectorDeg(r) - 90;

        assertTrue(Math.abs(angle -  angle2) < 0.001f);
    }

}