package no.gj2016.utils;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Matrix4;

public class SaneCamera {
    private float x;
    private float y;
    private float aspect;
    private float viewportWidth;

    public SaneCamera(float viewportWidth)
    {
        this.x = 0.0f;
        this.y = 0.0f;
        this.aspect = 1.0f;
        this.viewportWidth = viewportWidth;
    }

    public void setAspect(float aspect)
    {
       this.aspect = aspect;
    }

    public Matrix4 getProjectionMatrix()
    {
        float wx = this.viewportWidth / 2.0f;
        float wy = wx / aspect;

        Matrix4 m = new Matrix4();
        m.setToOrtho(-wx, +wx, -wy, +wy, -1.0f, +1.0f);

        return m;
    }

    public void translate(float x, float y)
    {
        this.x += x;
        this.y += y;
    }

    public Matrix4 getTransformMatrix()
    {
        Matrix4 m = new Matrix4();
        m.translate(-x, -y, 0.0f);
        return m;
    }

    public OrthographicCamera getTilemapCamera(float tileSize)
    {
        float wx = viewportWidth * tileSize;
        float wy = wx / aspect;
        OrthographicCamera camera = new OrthographicCamera(wx, wy);
        camera.translate(x * tileSize, y * tileSize);
        camera.update();

        return camera;
    }
}
