package no.gj2016.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import no.gj2016.Line;

import java.util.List;

/**
 * Created by maasbak on 30.01.2016.
 */
public class LineDrawer {
    private Color lineColor;
    private float lineWidth;

    public LineDrawer(Color lineColor, float lineWidth) {
        this.lineColor = lineColor;
        this.lineWidth = lineWidth;
    }

    public void Draw(List<Line> lines) {
        // Draw cool lines
        ShapeRenderer sr = new ShapeRenderer();
        sr.begin(ShapeRenderer.ShapeType.Line);
        sr.setColor(this.lineColor);
        for (Line line: lines) {
            sr.rectLine(line.first, line.second, this.lineWidth);
            // sr.line(line.first.x, line.first.y, line.second.x, line.second.y);
        }
        sr.end();
    }
}
