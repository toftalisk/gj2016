package no.gj2016.utils;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by are on 30.01.16.
 */
public class AngleTool {
    public static float angleFromVectorRad(Vector2 v)
    {
        if (Math.abs((double)v.x) < 0.00001f)
        {
            if (v.y > 0.0f) {
                return (float)Math.PI / 2.0f;
            } else {
                return (float)Math.PI / -2.0f;
            }
        }
        float angle = (float)Math.atan(v.y / v.x);

        if (v.x < 0.0f)
        {
            return (float)Math.PI + angle;
        }
        return angle;
    }

    public static Vector2 rotateVectorRad(Vector2 v, float rad)
    {
        return new Vector2(
                (float)((v.x * Math.cos(rad)) - (v.y * Math.sin(rad))),
                (float)((v.y * Math.cos(rad)) + (v.x * Math.sin(rad)))
        );
    }

    public static Vector2 rotateVectorDeg(Vector2 v, float deg)
    {
        return rotateVectorRad(v, toRad(deg));
    }

    public static float angleFromVectorDeg(Vector2 v)
    {
        return angleFromVectorRad(v) * 180.0f / (float)Math.PI;
    }

    public static float toRad(float deg)
    {
        return (deg / 180.0f) * (float)Math.PI;
    }

    public static float toDeg(float rad)
    {
        return (rad * 180.0f) / (float)Math.PI;
    }

    public static float distanceRad(float a, float b)
    {
        float diff = a - b;

        while (diff > Math.PI) diff -= Math.PI;
        while (diff < -Math.PI) diff += Math.PI;

        return diff;
    }

    public static float distanceDeg(float a, float b)
    {
        return toDeg(distanceRad(toRad(a),toRad(b)));
    }
}
