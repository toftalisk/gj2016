package no.gj2016;

import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import junit.framework.TestCase;

import java.util.ArrayList;

public class RitualVerifierTest extends TestCase {

    // test method to add two values
    public void testBasicSquareShape(){
        I i = new I();
        L l = new L();
        Square s = new Square();

        ArrayList<Vector2> result = new ArrayList<Vector2>();

        RitualVerifyer ritualVerifyer = new RitualVerifyer();
        ritualVerifyer.updateRitual(s, s.getPoints());
        ritualVerifyer.updateRitual(s, l.getPoints());
        ritualVerifyer.updateRitual(s, i.getPoints());

    }
    // test method to add two values
    public void testBasicIShape(){
        I i = new I();
        L l = new L();

        ArrayList<Vector2> result = new ArrayList<Vector2>();

        RitualVerifyer ritualVerifyer = new RitualVerifyer();
        ritualVerifyer.updateRitual(i, i.getPoints());
        assertTrue(i.getStrength() > 0.0f);
        ritualVerifyer.updateRitual(i, l.getPoints());

    }

    public void testBasicLShape(){
        L l = new L();

        ArrayList<Vector2> result = new ArrayList<Vector2>();

        RitualVerifyer ritualVerifyer = new RitualVerifyer();
        ritualVerifyer.updateRitual(l, l.getPoints());
    }

    public void testShuffledPermutationLShape(){
        L l = new L();

        ArrayList<Vector2> result = new ArrayList<Vector2>();

        ArrayList<Vector2> testPoints = new ArrayList<Vector2>();
        testPoints.add(l.getPoints().get(3));
        testPoints.add(l.getPoints().get(1));
        testPoints.add(l.getPoints().get(2));
        testPoints.add(l.getPoints().get(0));

        RitualVerifyer ritualVerifyer = new RitualVerifyer();
        ritualVerifyer.updateRitual(l, testPoints);;

        // This should not pass as the first and last player is in the same spot
        testPoints.remove(3);
        testPoints.add(l.getPoints().get(3));
        ritualVerifyer.updateRitual(l, testPoints);;
    }

    public void testDistortedLShape() {
        L l = new L();

        ArrayList<Vector2> result = new ArrayList<Vector2>();

        ArrayList<Vector2> testPoints = new ArrayList<Vector2>(l.getPoints());

        testPoints.get(1).add(RitualVerifyer.KUSHAARET * 0.55f, 0.0f);

        RitualVerifyer ritualVerifyer = new RitualVerifyer();
        ritualVerifyer.updateRitual(l, testPoints);;

        testPoints.get(1).add(RitualVerifyer.KUSHAARET * 0.55f, 0.0f);

        ritualVerifyer.updateRitual(l, testPoints);
    }

    public void assertTransformedShape(float angle, float scale, Vector2 translate) {
        L l = new L();

        ArrayList<Vector2> result = new ArrayList<Vector2>();

        Matrix3 matrix = new Matrix3();
        matrix.rotate(angle);

        ArrayList<Vector2> testPoints = new ArrayList<Vector2>();
        for (Vector2 v : l.getPoints())
        {
            Vector3 v3 = new Vector3(v.x, v.y, 1.0f).mul(matrix);
            testPoints.add(new Vector2(v3.x, v3.y));
        }

        RitualVerifyer ritualVerifyer = new RitualVerifyer();
        ritualVerifyer.updateRitual(l, testPoints);;
        assertTrue(l.getStrength() < 1.0f);

        testPoints.get(1).add(RitualVerifyer.KUSHAARET*3, 0);
        ritualVerifyer.updateRitual(l, testPoints);;
    }

    public void testRotatedLShape() {
        Vector2 t = new Vector2(0.0f, 0.0f);
        assertTransformedShape(45, 1, t);
        assertTransformedShape(-45, 1, t);
        assertTransformedShape((float) Math.PI, 1, t);
        assertTransformedShape(298.3f, 1, t);
        assertTransformedShape(-1, 1, t);
        assertTransformedShape(1, 1, t);
        assertTransformedShape(2, 1, t);
    }

    public void testScaledRotatedAndTranslatedLShape() {
        Vector2 t = new Vector2(4.0f, 200.0f);
        assertTransformedShape(45, 1, t);
        assertTransformedShape(-45, 1, t);
        assertTransformedShape((float) Math.PI, 1, t);
        assertTransformedShape(298.3f, (float) Math.PI, t);
        assertTransformedShape(-1, 0.5f, t);
        assertTransformedShape(1, 4, t);
        assertTransformedShape(2, 0.12345f, t);
    }
}
