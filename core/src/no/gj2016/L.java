package no.gj2016;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import no.gj2016.particles.ParticleSquirter;
import no.gj2016.particles.RitualSpark;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by are on 30.01.16.
 */
public class L extends Ritual {

    ParticleSquirter particlesEffect = null;
    float total = 0.0f;

    @Override
    public List<Vector2> getPoints() {
        List<Vector2> points = new ArrayList<Vector2>();
        points.add(new Vector2(0.0f, 2.0f));
        points.add(new Vector2(0.0f, 1.0f));
        points.add(new Vector2(0.0f, 0.0f));
        points.add(new Vector2(1.0f, 0.0f));

        return points;
    }

    @Override
    public int getPrimaryIndex() {
        return 0;
    }

    @Override
    public int getSecondaryIndex() {
        return 3;
    }

    @Override
    public void drawMe(SpriteBatch batch, ShapeRenderer shapeRenderer) {
        if (strength == 0.f) {
            return;
        }
        else {
            List<Line> lines = new ArrayList<Line>();
            lines.add(new Line(playerPoints.get(0), playerPoints.get(1)));
            lines.add(new Line(playerPoints.get(1), playerPoints.get(2)));
            lines.add(new Line(playerPoints.get(2), playerPoints.get(3)));

            RitualSpark.spawnBeam(lines, strength, 0.0f, 1.0f, 1.0f);

            if (particlesEffect != null) {
                batch.begin();
                particlesEffect.draw(batch);
                batch.end();
            }
        }
    }

	@Override
	void update(float dt, EventManager eventManager) {

        if(strength == 0.0f) {

            total -= 0.001f;
            if (total <= 0)
                total = 0;

            if(particlesEffect != null)
                particlesEffect = null;
            return;
        }
        else {
            total += 0.2f *dt;
            if (total >= 1.001f)
                total = 1f;


            if (total >= 1.0f && eventManager.checkFlag("baby dead")) {
                eventManager.setFlag("door open");

                if(particlesEffect == null) {
                    Vector2 particlePos = getParticlePosFromPlayers();
                    particlesEffect = new ParticleSquirter("keyEffect.particle");
                    particlesEffect.repeatable = false;
                    particlesEffect.squirtAt(particlePos.x, particlePos.y);
                }

                particlesEffect.update(dt);
            }


        }

	}

    private Vector2 getParticlePosFromPlayers() {
        Vector2 particlePos = new Vector2();
        for(Vector2 v : playerPoints)
        {
            particlePos.add(v);
        }

        particlePos = particlePos.scl(1.0f/playerPoints.size());
        return particlePos;
    }
}
