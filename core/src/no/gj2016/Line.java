package no.gj2016;

import com.badlogic.gdx.math.Vector2;

import java.util.List;

/**
 * Created by mto on 30/01/2016.
 */
public class Line {
    public Vector2 first;
    public Vector2 second;

    public Line (Vector2 first, Vector2 second) {
        this.first = first;
        this.second = second;
    }
}
