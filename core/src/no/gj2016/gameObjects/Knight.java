package no.gj2016.gameObjects;

import com.badlogic.gdx.math.Vector2;
import no.gj2016.EventManager;
import no.gj2016.events.SeekInfo;
import no.gj2016.sound.SoundHandler;
import no.gj2016.utils.AngleTool;

/**
 * Created by are on 31.01.16.
 */
public class Knight extends Enemy {
    private float reverseMode;
    private float charge;

    public Knight(String imageFilename, SoundHandler soundHandler)
    {
        super(imageFilename, 2.0f, soundHandler);
        this.reverseMode = 0.0f;
    }

    @Override
    public void update(float dt, EventManager eventManager) {
        float baseSpeed = 30.0f * eventManager.getSlowTime();
        if (this.reverseMode > 0) {
            moveLocal(new Vector2(0.0f, -dt * 3 * baseSpeed));
            reverseMode -= dt;
            if (reverseMode < 0) {
                setChargeMode();
            }
        } else if (this.charge > 0) {
            moveLocal(new Vector2(0.0f, dt * baseSpeed * 5));
            charge -= dt;
        } else {
            moveLocal(new Vector2(0.0f, dt * baseSpeed));
        }
    }

    @Override
    public boolean seekEnabled() {
        return true;
    }

    @Override
    public SeekInfo getSeekInfo(String type) {
        if (type.equalsIgnoreCase("player"))
        {
            return new SeekInfo(10.0f, true);
        }
        return null;
    }

    @Override
    public void onSeekFound(GameObject other) {
        this.angle = AngleTool.angleFromVectorDeg(other.getPosition().sub(getPosition())) - 90.0f;
        this.setChargeMode();
    }

    private void setChargeMode() {
        this.charge = 0.5f;
    }

    @Override
    public void collidesWith(GameObject other, EventManager event) {
        super.collidesWith(other, event);
        if(other.getType().equalsIgnoreCase("player")) {
            ((Monk) other).takeDamage(10.0f, event);
            this.setReverseMode();
        } else {
            this.rotate(170.0f);
        }
    }

    private void setReverseMode() {
        this.charge = 0.0f;
        this.reverseMode=0.5f;
    }
}
