package no.gj2016.gameObjects;

import no.gj2016.EventManager;

/**
 * Created by mto on 31/01/2016.
 */
public class Wall extends GameObject {
    private  float x;
    private  float y;
    public Wall(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public float getX() {
        return 0;
    }

    @Override
    public float getY() {
        return 0;
    }

    @Override
    public void collidesWith(GameObject other, EventManager event) {

    }

    @Override
    public String getType() {
        return "wall";
    }
}
