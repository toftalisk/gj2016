package no.gj2016.gameObjects;

import no.gj2016.EventManager;
import no.gj2016.particles.ParticleSquirterSingleton;
import no.gj2016.sound.SoundHandler;


/**
 * Created by andreas on 31/01/16.
 */
public class Farmer extends SimpleSpriteObject {

	private String objName = new String();

	public Farmer(String farmerName, SoundHandler soundHandler) {
		super("2d/" + farmerName + ".png", 1.4f, soundHandler);
		objName = farmerName;
	}

	@Override
	public void onKilled(EventManager eventManager) {
		super.onKilled(eventManager);
		ParticleSquirterSingleton.triggerSingleEffectAtPoint(objName + "Soul.particle", getBody().getPosition());
	}

}
