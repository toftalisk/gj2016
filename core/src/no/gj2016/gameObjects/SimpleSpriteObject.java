package no.gj2016.gameObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Affine2;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import no.gj2016.EventManager;
import no.gj2016.sound.GameSound;
import no.gj2016.sound.SoundHandler;
import no.gj2016.utils.AngleTool;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.badlogic.gdx.graphics.Color.FOREST;

/**
 * Created by are on 29.01.16.
 */
public class SimpleSpriteObject extends GameObject {
    private float MAX_HEALTH = 100.f;
    private String type;
    private Texture texture;
    private TextureRegion region;
    protected float angle;
    private float size;

    private final float speedAmplifier = 400f;
    private Affine2 pretransform;
    private Body body;
    private SoundHandler soundHandler;
    
    private float health = 100.0f;
    
    private float initialX, initialY;

    protected boolean toRemove = false;
    protected boolean removed = false;
    protected GameSound damageSound = null;
    protected Date lastDamageTime = null;
    boolean inactive = false;
    protected static long damageGraceTimeMillis = 1000;
    
    private Vector2 continuousMove;
    private boolean isContinuosMove = false;

    public SimpleSpriteObject(String image_filename, float size, SoundHandler soundHandler)

    {
        texture = new Texture(image_filename);
        this.soundHandler = soundHandler;

        pretransform = new Affine2();
        pretransform.translate(size / -2.0f, size / -2.0f);

        region = new TextureRegion(texture);

        this.angle = 0.0f;
        this.size = size;
    }

    public Map<String, String> properties = new HashMap<String, String>();

    public static float ROTATE_TRESHOLD = 0.1f;

    public void update(float dt, EventManager eventManager)
    {
    	if(isContinuosMove)
    	{
    		move(continuousMove);
    	}
    }

    public void move(Vector2 movement) {
        float x =  movement.x * Gdx.graphics.getDeltaTime() * speedAmplifier;
        float y =  movement.y * Gdx.graphics.getDeltaTime() * speedAmplifier;
        //setPos(x, y);
        //body.applyForceToCenter(x, y, true);
        body.setLinearVelocity(x, y);
    }

    /// Move according to the local coordinate system. i.e +y forward -x left etc
    public void moveLocal(Vector2 m)
    {
        move(AngleTool.rotateVectorDeg(m, this.angle));
    }

    public void setPos(float x, float y)
    {
    	if (body == null)
    	{
    		initialX = x;
    		initialY = y;
    	}
    	
    	else {
        body
                .getPosition()
                .set(x,y);
    	}
    }

    @Override
    public float getX() {
        return body.getPosition().x;
    }

    @Override
    public float getY() {
        return body.getPosition().y;
    }

    public float getAngle()
    {
        return this.angle;
    }

    @Override
    public void collidesWith(GameObject other, EventManager event) {

    }

    public void drawMe(Batch batch, ShapeRenderer shapeRenderer) {
        if (inactive) return;
        Affine2 transform = new Affine2();
        transform.translate(getX() , getY());
        transform.rotate(angle);

        batch.begin();
        batch.draw(region, size, size, transform.mul(pretransform));
        batch.end();

        float health_bar_length = health / MAX_HEALTH;
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(FOREST);
        float x = getX() -0.5f;
        float y = getY() +0.7f;
        shapeRenderer.rectLine(x, y, x +health_bar_length, y, 0.3f);
        shapeRenderer.end();
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Body getBody() {
        return body;
    }

    public boolean alive() {
        return health >= 0;
    }

    public void rotate(float angle)
    {
        this.angle += angle;
    }

	public void takeDamage(float damage, EventManager eventManager)
    {
        if (getType() == "baby") {
            health -= damage/3;
        } else {
            health -= damage;
        }
        if (health <= 0.0f)
        {
            onKilled(eventManager);
        }

        if (!isRemoved()) {
            // Lots of sound logic, check if object uses sound first
            if (this.damageSound != null) {
                // Remember when the character took damage, so the sounds aren't played a million times
                if (lastDamageTime == null) {
                    this.soundHandler.PlaySound(this.damageSound, 50.0f);
                    lastDamageTime = new Date();
                } else {
                	// Check if enough time has passed since the sound was played
                	long timeDiff = new Date().getTime() - lastDamageTime.getTime();
                	if (timeDiff > damageGraceTimeMillis) {
                		lastDamageTime = null;
                	}
                }
            }
        }
	}

    public void onKilled(EventManager eventManager)
    {
        this.remove();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void remove() {
        toRemove = true;
    }

    public boolean shouldRemove() {
        return toRemove;
    }

    public void setRemoved() {
        removed = true;
    }

    public boolean isRemoved() {
        return removed;
    }

    public void setInactive() {
        inactive = true;
    }

	public void setContinuousMove(Vector2 direction_vector) {
		isContinuosMove = true;
		continuousMove = direction_vector;
	}
}
