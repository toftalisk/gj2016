package no.gj2016.gameObjects;

import com.badlogic.gdx.math.Vector2;
import no.gj2016.EventManager;
import no.gj2016.events.SeekInfo;

public abstract class GameObject {
    public abstract float getX();
    public abstract float getY();
    public abstract void collidesWith(GameObject other, EventManager event);
    public abstract String getType();

    public Vector2 getPosition()
    {
        return new Vector2(getX(), getY());
    }

    public float getDistance(GameObject other)
    {
        return new Vector2(getX(), getY()).sub(other.getX(), other.getY()).len();
    }

    public boolean seekEnabled()
    {
        return false;
    }

    public SeekInfo getSeekInfo(String type)
    {
        return null;
    }
    public void onSeekFound(GameObject other)
    {

    }
}
