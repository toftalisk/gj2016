package no.gj2016.gameObjects;

import no.gj2016.userInput.UserInput;

/**
 * Created by andreas on 30/01/16.
 */
public interface PlayerCharacter {
    void set(UserInput userInput);
}
