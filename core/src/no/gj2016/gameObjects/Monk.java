package no.gj2016.gameObjects;

import com.badlogic.gdx.math.Vector2;
import no.gj2016.EventManager;
import no.gj2016.particles.ParticleSquirterSingleton;
import no.gj2016.sound.GameSound;
import no.gj2016.sound.SoundHandler;
import no.gj2016.userInput.UserInput;
import no.gj2016.utils.AngleTool;

public class Monk extends SimpleSpriteObject implements PlayerCharacter {
    UserInput userInput;
    private String monkName = new String();

    public Monk(String monkName, SoundHandler soundHandler)
    {
        super("2d/" + monkName + ".png", 1.4f, soundHandler);
        this.monkName = monkName;
        this.damageSound = GameSound.AUCH;
    }

    @Override
    public void update(float dt, EventManager eventManager) {
        if(userInput != null)
            move(userInput.getMovement());
    }

    @Override
    public void collidesWith(GameObject other, EventManager eventManager) {
        super.collidesWith(other, eventManager);

        if (other.getType().equalsIgnoreCase("door") && eventManager.checkFlag("door open"))
        {
            eventManager.increaseWinState();
            remove();
        }
    }

    @Override
    public void move(Vector2 movement) {
        if (movement.len() > ROTATE_TRESHOLD)
        {
            this.angle = AngleTool.angleFromVectorDeg(movement) - 90.0f;
        }
        super.move(movement);
    }

    public void onKilled(EventManager eventManager) {
        super.onKilled(eventManager);

        ParticleSquirterSingleton.triggerSingleEffectAtPoint(monkName + "Soul.particle", getBody().getPosition());

    }

    public void set(UserInput userInput) {
        this.userInput = userInput;
    }

}