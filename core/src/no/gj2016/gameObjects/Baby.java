package no.gj2016.gameObjects;

import no.gj2016.EventManager;
import no.gj2016.sound.GameSound;
import no.gj2016.sound.SoundHandler;
import no.gj2016.particles.ParticleSquirterSingleton;

/**
 * Created by are on 31.01.16.
 */
public class Baby extends SimpleSpriteObject {
    public Baby(String imageFilename, SoundHandler soundHandler)
    {
        super(imageFilename, 2.0f, soundHandler);
        this.damageSound = GameSound.BABY;
    }

    @Override
    public void onKilled(EventManager eventManager) {
        super.onKilled(eventManager);
        ParticleSquirterSingleton.triggerSingleEffectAtPoint("babySoul.particle", getBody().getPosition());
        eventManager.setFlag("baby dead");
        eventManager.increaseBabySouls();
    }
}
