package no.gj2016.gameObjects;

import no.gj2016.EventManager;
import no.gj2016.particles.ParticleSquirterSingleton;
import no.gj2016.sound.SoundHandler;

/**
 * Created by are on 31.01.16.
 */
public abstract class Enemy extends SimpleSpriteObject {
    Enemy(String imageFilename, float size, SoundHandler soundHandler)
    {
        super(imageFilename, size, soundHandler);
    }

    @Override
    public void onKilled(EventManager eventManager) {
        super.onKilled(eventManager);
        ParticleSquirterSingleton.triggerSingleEffectAtPoint("bloodsplatt.particle", getBody().getPosition());
    }
}
