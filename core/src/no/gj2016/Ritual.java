package no.gj2016;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

import no.gj2016.EventManager;


public abstract class Ritual {
    protected float strength;
    protected List<Vector2> playerPoints;
    public Ritual() {
        strength = 0.0f;
        playerPoints = new ArrayList<Vector2>();

    }
    abstract List<Vector2> getPoints();
    abstract int getPrimaryIndex();
    abstract int getSecondaryIndex();
    abstract void drawMe(SpriteBatch batch, ShapeRenderer shapeRenderer);
    abstract void update(float dt, EventManager eventManager);

    void setStrength(float v) {
        this.strength = v;
    };

    float getStrength() {
        return this.strength;
    }

    void setPlayerPoints(List<Vector2> selectedPlayerPoints) {
        this.playerPoints = selectedPlayerPoints;
    };

    public void updateStrength(List<Vector2> playerPoints, float strength) {
        this.playerPoints = playerPoints;
        this.strength = strength;
    }

}
