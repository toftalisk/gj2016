package no.gj2016.events;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import no.gj2016.gameObjects.GameObject;

/**
 * Created by are on 31.01.16.
 */
public class SeekRaycastCallback implements RayCastCallback {
    private GameObject target;
    private int success;
    private int count = 0;

    private static final int UNKNOWN = 0;
    private static final int FAIL = 1;
    private static final int PASS = 2;

    public SeekRaycastCallback(GameObject target)
    {
        this.target = target;
        this.success = UNKNOWN;
        this.count = 0;
    }

    public int getCount()
    {
        return count;
    }

    public boolean wasSuccessful()
    {
        return success == PASS;
    }
    @Override
    public float reportRayFixture(Fixture fixture, Vector2 point, Vector2 normal, float fraction) {
        Object ob = fixture.getBody().getUserData();

        if (ob != target)
        {
            this.success = FAIL;
        } else if (this.success != FAIL)
        {
            this.success = PASS;
        }

        count ++;

        return fraction;
    }
}
