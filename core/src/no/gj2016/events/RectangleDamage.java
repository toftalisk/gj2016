package no.gj2016.events;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by are on 31.01.16.
 */
public class RectangleDamage implements AreaDamage {
    private float damage;
    private Rectangle rectangle;
    private Set<String> immuneTypes;

    public RectangleDamage(Rectangle rectangle, float damage)
    {
        this.rectangle = rectangle;
        this.damage = damage;
        this.immuneTypes = new HashSet<String>();
    }

    public void addImmune(String type)
    {
        immuneTypes.add(type);
    }

    @Override
    public float getDamage() {
        return damage;
    }

    @Override
    public boolean shouldDamage(Vector2 p, String type) {

        return this.rectangle.contains(p) && !immuneTypes.contains(type);
    }
}
