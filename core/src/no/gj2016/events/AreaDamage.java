package no.gj2016.events;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by are on 31.01.16.
 */
public interface AreaDamage {
    float getDamage();
    boolean shouldDamage(Vector2 p, String type);
}
