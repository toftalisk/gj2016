package no.gj2016.events;

/**
 * Created by are on 31.01.16.
 */
public class SeekInfo {
    private float radius;
    private boolean lineOfSight;

    public SeekInfo(float radius, boolean lineOfSight)
    {
        this.radius = radius;
        this.lineOfSight = lineOfSight;
    }

    public float getRadius()
    {
        return radius;
    }

    public boolean useLineOfSight()
    {
        return lineOfSight;
    }
}
