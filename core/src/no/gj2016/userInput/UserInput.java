package no.gj2016.userInput;

import com.badlogic.gdx.math.Vector2;
/**
 * Created by andreas on 30/01/16.
 */
public interface UserInput {
    public Vector2 getMovement();
    public boolean fireTriggered();
}
