package no.gj2016.userInput;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;


/**
 * Created by andreas on 30/01/16.
 */
public class GamePad implements ControllerListener, UserInput {

    private boolean hasControllers = false;
    private Vector2 accelerationVector = new Vector2(0, 0);

    public static final float DEAD_ZONE = 0.4f;

    @Override
    public Vector2 getMovement() {
        if (accelerationVector.len() < DEAD_ZONE)
            return new Vector2(0.0f, 0.0f);
        return accelerationVector;
    }

    @Override
    public boolean fireTriggered() {
        // @todo implement yo!
        return false;
    }
    // connected and disconnect dont actually appear to work for XBox 360 controllers.
    @Override
    public void connected(Controller controller) {
        hasControllers = true;
    }

    @Override
    public void disconnected(Controller controller) {
        hasControllers = false;
    }

    @Override
    public boolean buttonDown(Controller controller, int buttonCode) {
        if(buttonCode == XBox360Pad.BUTTON_Y)
            Gdx.app.log("Gamepad","BUTTON_Y");
        if(buttonCode == XBox360Pad.BUTTON_A)
            Gdx.app.log("Gamepad","BUTTON_A");
        if(buttonCode == XBox360Pad.BUTTON_X)
            Gdx.app.log("Gamepad","BUTTON_X");
        if(buttonCode == XBox360Pad.BUTTON_B)
            Gdx.app.log("Gamepad","BUTTON_B");

        if(buttonCode == XBox360Pad.BUTTON_LB)
            Gdx.app.log("Gamepad","BUTTON_LB");
        if(buttonCode == XBox360Pad.BUTTON_RB)
            Gdx.app.log("Gamepad","BUTTON_RB");
        return false;
    }

    @Override
    public boolean buttonUp(Controller controller, int buttonCode) {
        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int axisCode, float value) {
        // This is your analog stick
        // Value will be from -1 to 1 depending how far left/right, up/down the stick is
        // For the Y translation, I use a negative because I like inverted analog stick
        // Like all normal people do! ;)

        // Left Stick NOTE that X and Y is swapped.
        if(axisCode == XBox360Pad.AXIS_LEFT_X)
        {
            this.accelerationVector.x = value;
        }

        if(axisCode == XBox360Pad.AXIS_LEFT_Y)
        {
            this.accelerationVector.y = -value;
        }

        return false;
    }

    @Override
    public boolean povMoved(Controller controller, int povCode, PovDirection value) {
        // This is the dpad
        switch(value) {
            case center:
                accelerationVector.set(0.0f, 0.0f);
                Gdx.app.log("Gamepad", "POV = center");
                break;
            case north:
                accelerationVector.set(0.0f, 1.0f);
                Gdx.app.log("Gamepad", "POV = north");
                break;
            case northEast:
                accelerationVector.set(0.707f, 0.707f);
                Gdx.app.log("Gamepad", "POV = north-east");
                break;
            case east:
                accelerationVector.set(1.0f, 0.0f);
                Gdx.app.log("Gamepad", "POV = east");
                break;
            case southEast:
                accelerationVector.set(0.707f, -0.707f);
                Gdx.app.log("Gamepad", "POV = southEast");
                break;
            case south:
                accelerationVector.set(0.0f, -1.0f);
                Gdx.app.log("Gamepad", "POV = south");
                break;
            case southWest:
                accelerationVector.set(-0.707f, -0.707f);
                Gdx.app.log("Gamepad", "POV = southWest");
                break;
            case west:
                accelerationVector.set(-1.0f, 0.0f);
                Gdx.app.log("Gamepad", "POV = west");
                break;
            case northWest:
                accelerationVector.set(-0.707f, 0.707f);
                Gdx.app.log("Gamepad", "POV = northWest");
                break;
        }

        return false;
    }

    @Override
    public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value) {
        return false;
    }
}
