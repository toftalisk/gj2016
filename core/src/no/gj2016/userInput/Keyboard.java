package no.gj2016.userInput;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.controllers.PovDirection;

import java.util.ArrayList;

/**
 * Created by andreas on 30/01/16.
 *
 * Note tha
 */
public class Keyboard implements UserInput {
    private enum ButtonCombination {
                ARROW_KEYS,
                WASD,
                YGHJ,
                NUMPAD
    }

    static int index = 0;

    public static void releaseKeyboards()
    {
        index = 0;
    }

    public static UserInput createKeyboard() {
        ArrayList<ButtonCombination> keyboardCreateOrder = new ArrayList<ButtonCombination>(){{
            add(ButtonCombination.ARROW_KEYS);
            add(ButtonCombination.WASD);
            add(ButtonCombination.NUMPAD);
            add(ButtonCombination.YGHJ);
        }};

        Keyboard keyboard = new Keyboard();
        switch(keyboardCreateOrder.get(index))
        {
            case ARROW_KEYS:
                keyboard.UP = Input.Keys.UP;
                keyboard.RIGHT = Input.Keys.RIGHT;
                keyboard.DOWN = Input.Keys.DOWN;
                keyboard.LEFT = Input.Keys.LEFT;
                break;
            case WASD:
                keyboard.UP = Input.Keys.W;
                keyboard.RIGHT = Input.Keys.D;
                keyboard.DOWN = Input.Keys.S;
                keyboard.LEFT = Input.Keys.A;
                break;
            case YGHJ:
                keyboard.UP = Input.Keys.Y;
                keyboard.RIGHT = Input.Keys.J;
                keyboard.DOWN = Input.Keys.H;
                keyboard.LEFT = Input.Keys.G;
                break;
            case NUMPAD:
                keyboard.UP = Input.Keys.NUMPAD_8;
                keyboard.RIGHT = Input.Keys.NUMPAD_6;
                keyboard.DOWN = Input.Keys.NUMPAD_5;
                keyboard.LEFT = Input.Keys.NUMPAD_4;
                break;
        }

        index++;
        return keyboard;
    }

    private int UP      = 0;
    private int RIGHT   = 1;
    private int DOWN    = 2;
    private int LEFT    = 3;


    int keys[] = new int[4];

    public Vector2 getMovement() {
        switch(getKeyboardPov()) {
            case north:
                Gdx.app.debug("Keyboard", "POV = north");
                return new Vector2(0.0f, 1.0f);
            case northEast:
                Gdx.app.debug("Keyboard", "POV = north-east");
                return new Vector2(0.707f, 0.707f);
            case east:
                Gdx.app.debug("Keyboard", "POV = east");
                return new Vector2(1.0f, 0.0f);
            case southEast:
                Gdx.app.debug("Keyboard", "POV = southEast");
                return new Vector2(0.707f, -0.707f);
            case south:
                Gdx.app.debug("Keyboard", "POV = south");
                return new Vector2(0.0f, -1.0f);
            case southWest:
                Gdx.app.debug("Keyboard", "POV = southWest");
                return new Vector2(-0.707f, -0.707f);
            case west:
                Gdx.app.debug("Keyboard", "POV = west");
                return new Vector2(-1.0f, 0.0f);
            case northWest:
                Gdx.app.debug("Keyboard", "POV = northWest");
                return new Vector2(-0.707f, 0.707f);
            case center:
            default:
                return new Vector2(0.0f, 0.0f);
        }
    }

    public boolean fireTriggered() {
        return false;
    }

    private PovDirection getKeyboardPov() {
        boolean upPressed = Gdx.input.isKeyPressed(UP);
        boolean rightPressed = Gdx.input.isKeyPressed(RIGHT);
        boolean downPressed = Gdx.input.isKeyPressed(DOWN);
        boolean leftPressed = Gdx.input.isKeyPressed(LEFT);
        if(upPressed && rightPressed)
            return PovDirection.northEast;
        else if(upPressed && leftPressed)
            return PovDirection.northWest;
        else if(upPressed)
            return PovDirection.north;
        else if(downPressed && rightPressed)
            return PovDirection.southEast;
        else if(downPressed && leftPressed)
            return PovDirection.southWest;
        else if(downPressed)
            return PovDirection.south;
        else if(rightPressed)
            return PovDirection.east;
        else if(leftPressed)
            return PovDirection.west;
        else
            return PovDirection.center;
    }
}
