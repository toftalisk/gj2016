package no.gj2016;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;


public class RitualVerifyer {

    public static final float KUSHAARET = 3.5f;
    public void updateRitual(Ritual theRitual, List<Vector2> playerPoints){
        if(theRitual.getPoints().size() != playerPoints.size()) {
            throw new FuckYouException("points in ritual does not match number of player");
        }
        theRitual.setStrength(0.0f);
        recursiveCheck(theRitual, playerPoints, new ArrayList<Vector2>());
    }


    /**
     * This function seems scary, but all it does is call checkPermutation for all 16 possible permutation of players
     * @param theRitual
     * @param playerPoints
     * @param selectedPlayerPoints
     * @return
     */
    private void recursiveCheck(
        Ritual theRitual,
        List<Vector2> playerPoints,
        List<Vector2> selectedPlayerPoints
    ) {
        if (playerPoints.size() == 0) {
            checkPermutation(theRitual, selectedPlayerPoints);
        }
        for (int i=0; i<playerPoints.size(); i++) {
            List<Vector2> clonedPlayerPoints = new ArrayList<Vector2>(playerPoints);
            List<Vector2> clonedSelectedPlayerPoints = new ArrayList<Vector2>(selectedPlayerPoints);

            clonedSelectedPlayerPoints.add(clonedPlayerPoints.remove(i));
            recursiveCheck(
                theRitual,
                clonedPlayerPoints,
                clonedSelectedPlayerPoints
            );
        }
    }

    private void checkPermutation(
        Ritual theRitual,
        List<Vector2> playerPoints
    ) {
        List<Vector2> points = theRitual.getPoints();

        int primary = theRitual.getPrimaryIndex();
        int secondary = theRitual.getSecondaryIndex();

        //Get
        Vector2 ritual = new Vector2(points.get(secondary)).sub(points.get(primary));
        Vector2 player = new Vector2(playerPoints.get(secondary)).sub(playerPoints.get(primary));

        if (player.len() < 4.0f) return;

        float distanceAdjustedKushaar = KUSHAARET * player.len() / 10;

        Vector2 ritualUnit = new Vector2(ritual).scl(1.0f/ritual.len());
        Vector2 playerUnit = new Vector2(player).scl(1.0f/player.len());



        float dotProduct = ritualUnit.dot(playerUnit);
        float angle = (float) Math.acos(dotProduct);

        if (ritualUnit.crs(playerUnit) > 0.0f)
        {
            angle *= -1.0f;
        }

        final float scale = player.len() / ritual.len();
        Vector2 primaryRitualPointScaledAndRotated = transform(points.get(primary), angle, scale);

        Vector2 translate = new Vector2(playerPoints.get(primary)).sub(primaryRitualPointScaledAndRotated);

        float maxDistance = 0;
        for (int i=0; i<playerPoints.size(); i++) {
            Vector2 ritualPoint = theRitual.getPoints().get(i);
            Vector2 playerPoint = playerPoints.get(i);
            Vector2 transformedRitualPoint = transform(ritualPoint, angle, scale).add(translate);
            float distance = getDistance(transformedRitualPoint, playerPoint);
            if (distance > maxDistance) {
                maxDistance = distance;
            }
        }
        float strength = distanceToStrength(maxDistance, distanceAdjustedKushaar);
        if (strength > 0)
        {
            Gdx.app.debug("RitualVerifier", "Ritual strength = " + strength);
            theRitual.updateStrength(playerPoints, strength);
        }
    }

    private float distanceToStrength(float maxDistance, float dak) {
        float tmp = dak - maxDistance;
        if (tmp < 0) return 0;
        else return (float) Math.pow(tmp / dak, 3.0f);
    }

    private float getDistance(Vector2 a, Vector2 b) {
        return a.sub(b).len2();
    }

    private Vector2 transform(Vector2 p, float angle, float scale) {
        return new Vector2(
            (float)((p.x * Math.cos(angle)) + (p.y * Math.sin(angle))),
            (float)((p.y * Math.cos(angle)) - (p.x * Math.sin(angle)))
        ).scl(scale);
    }

}
