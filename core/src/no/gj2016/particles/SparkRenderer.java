package no.gj2016.particles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by are on 30.01.16.
 */
public class SparkRenderer {
    private ArrayList<RitualSpark> sparks;

    private static SparkRenderer instance;
    public static SparkRenderer getInstance()
    {
        if (instance == null)
        {
            instance = new SparkRenderer();
        }

        return instance;
    }

    public void addSpark(RitualSpark spark)
    {
        sparks.add(spark);
    }

    private SparkRenderer()
    {
        sparks = new ArrayList<RitualSpark>();
    }

    public void update(float dt)
    {
        for (Iterator<RitualSpark> s = sparks.iterator(); s.hasNext();) {
            RitualSpark rs = s.next();
            if (!rs.update(dt)) {
                s.remove();

            }
        }
    }
    public void render(ShapeRenderer renderer)
    {
        Gdx.gl20.glEnable(GL20.GL_BLEND);
        renderer.begin(ShapeRenderer.ShapeType.Filled);
        for (RitualSpark s : sparks)
        {
            Vector2 p = s.calculatePosition();
            renderer.setColor(s.getR(), s.getG(), s.getB(), 0.2f);
            renderer.circle(p.x, p.y, 0.2f, 12);

        }
        renderer.end();
    }

}
