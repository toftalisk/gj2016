package no.gj2016.particles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by andreas on 31/01/16.
 */
public class ParticleSquirterSingleton {
	static ArrayList<ParticleSquirter> oneOffEffects = new ArrayList<ParticleSquirter>();

	// Utility functions for one-off effects
	public static void triggerSingleEffectAtPoint(String resource, Vector2 pos)
	{
		//Gdx.app.log("ParticleSquirterSingleton", "triggerSingleEffectAtPoint");
		ParticleSquirter oneOffEffect = new ParticleSquirter(resource);
		oneOffEffect.repeatable = false;
		oneOffEffect.squirtAt(pos);
		oneOffEffects.add(oneOffEffect);
	}

	public static void update(float dt)
	{
		removeCompletedEffects();

		for(ParticleSquirter p : oneOffEffects)
		{
			p.update(dt);
		}
	}

	public static void drawAll(Batch batch)
	{
		batch.begin();
		for(ParticleSquirter p : oneOffEffects)
		{
			p.draw(batch);
		}
		batch.end();
	}

	private static void removeCompletedEffects() {
		Iterator<ParticleSquirter> particleIterator = oneOffEffects.iterator();
		while(particleIterator.hasNext())
		{
			ParticleSquirter p = particleIterator.next();
			if(p.isComplete())
				particleIterator.remove();
		}
	}
}
