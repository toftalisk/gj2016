package no.gj2016.particles;

import com.badlogic.gdx.math.Vector2;
import no.gj2016.Line;

import java.util.List;
import java.util.Random;

/**
 * Created by are on 30.01.16.
 */
public class RitualSpark {
    private float time;
    private float duration;
    private Vector2 start;
    private Vector2 velocity;
    private Vector2 target;
    private float r;
    private float g;
    private float b;

    private RitualSpark(Vector2 start, Vector2 velocity, Vector2 target, float duration, float r, float g, float b)
    {
        this.start = new Vector2(start);
        this.target = new Vector2(target);
        this.velocity = new Vector2(velocity);
        this.time = 0.0f;
        this.duration = duration;

        this.r = r;
        this.g = g;
        this.b = b;
    }

    public static void spawn(Vector2 start, Vector2 velocity, Vector2 target, float duration, float r, float g, float b)
    {
        RitualSpark rs = new RitualSpark(start, velocity, target, duration, r, g, b);

        SparkRenderer.getInstance().addSpark(rs);
    }

    boolean update(float dt)
    {
        time += dt / duration;

        return (time < 1.0f);
    }

    float getR() { return this.r; }
    float getG() { return this.g; }
    float getB() { return this.b; }

    Vector2 calculatePosition()
    {
        Vector2 launchBased = new Vector2(start.x, start.y);
        launchBased.add(new Vector2(this.velocity).scl(time));

        Vector2 t = new Vector2(this.target);

        float rampedTime = time * time * time;
        launchBased.scl(1.0f - rampedTime);
        launchBased.add(t.scl(rampedTime));

        return launchBased;
    }

    public static void spawnBeam(List<Line> lines, float strength, float r, float g, float b)
    {
        Random random = new Random();

        float sparkDuration = (0.1f * strength) + (2.0f * (1.0f - strength));
        for (Line line : lines) {
            if (random.nextFloat() < strength) {
                RitualSpark.spawn(
                        line.first,
                        new Vector2().setToRandomDirection(),
                        line.second,
                        sparkDuration,
                        r, g, b
                );

                RitualSpark.spawn(
                        line.second,
                        new Vector2().setToRandomDirection(),
                        line.first,
                        sparkDuration,
                        r, g, b
                );
            }
        }
    }
}
