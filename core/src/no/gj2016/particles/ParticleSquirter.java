package no.gj2016.particles;

import java.lang.String;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter.ScaledNumericValue;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;
import no.gj2016.sound.SoundHandler;


public class ParticleSquirter {
	
	public ParticleEffect particleEffect;
	
	public boolean repeatable;
	private boolean drawable;
	private final float magicScale = 50.0f;
	
	public ParticleSquirter(String resource)
	{		
        particleEffect = new ParticleEffect();
 		particleEffect.load(Gdx.files.internal("2d/" + resource),Gdx.files.internal("2d"));		
		particleEffect.scaleEffect(1.0f / magicScale);
		particleEffect.getEmitters().first().setPosition(0.0f, 0.0f);
		repeatable = false;
		drawable = false;
	}
	

	public void update(float dt) {		
		particleEffect.update(dt);		
	}

	public void draw(Batch batch) {
				
		if (drawable) {
			particleEffect.draw(batch);
		}
		
		if (particleEffect.isComplete() && repeatable)
		{
			particleEffect.reset();
		}
		else if (particleEffect.isComplete())
		{
			drawable = false;
		}
	}

	public void squirtAt(Vector2 vec) {
		squirtAt(vec.x, vec.y);
	}

	public void squirtAt(float x, float y)
	{
 		particleEffect.getEmitters().first().setPosition(x, y);
 	    particleEffect.start();
 	    drawable = true;
	}
	
	public void stopSquirting()
	{
		drawable = false;		
	}

	public void setAngle(float direction) {
		Gdx.app.debug("PartileSquirter", "new angle was set to " + direction);
		for(ParticleEmitter emitter : particleEffect.getEmitters()) {
			ScaledNumericValue val = emitter.getAngle();
			val.setHigh(direction);
			val.setLow(direction);
		}
	}

	public boolean isComplete()
	{
		return particleEffect.isComplete();
	}
}
