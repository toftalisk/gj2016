package no.gj2016;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import no.gj2016.events.RectangleDamage;
import no.gj2016.particles.ParticleSquirter;
import no.gj2016.particles.RitualSpark;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mto on 30/01/2016.
 */
public class Hourglass extends Ritual{
    private float timeslow = 1;
    
    
    ParticleSquirter particle = null;

    @Override
    public List<Vector2> getPoints() {
        List<Vector2> points = new ArrayList<Vector2>();
        points.add(new Vector2(0.0f, 0.0f));
        points.add(new Vector2(1.0f, 0.0f));
        points.add(new Vector2(0.0f, 3.0f));
        points.add(new Vector2(1.0f, 3.0f));

        return points;
    }

    @Override
    public int getPrimaryIndex() {
        return 0;
    }

    @Override
    public int getSecondaryIndex() {
        return 3;
    }

    @Override
    public void drawMe(SpriteBatch batch, ShapeRenderer shapeRenderer) {
        if (strength == 0.f) return;

        List<Line> lines = new ArrayList<Line>();

        //cross
        lines.add(new Line(playerPoints.get(0), playerPoints.get(3)));
        lines.add(new Line(playerPoints.get(1), playerPoints.get(2)));

        //bottom
        lines.add(new Line(playerPoints.get(0), playerPoints.get(1)));

        //top
        lines.add(new Line(playerPoints.get(3), playerPoints.get(2)));

        RitualSpark.spawnBeam(lines, strength, 0.9f, 0.9f, 0.9f);
        
        if (particle != null)
        {
			batch.begin();
        	particle.draw(batch);
			batch.end();
        } 
    }

    @Override
    void update(float dt, EventManager eventManager) {
        timeslow -= 0.1 * strength * dt;
        if (timeslow <= 0.0f) {
            timeslow = 0.0f;
        }
        if (strength == 0 && timeslow < 1) {
            timeslow += 0.02 * dt;
        }
        eventManager.setTimeSlow(timeslow);
        
        if (strength >= 0.4f)
		{
			Vector2 center = new Vector2(0.0f, 0.0f);			
			for (Vector2 point: playerPoints)
			{
				center.add(point);
			}			
			center.scl(1.0f/playerPoints.size());					
			
			if (particle == null)
			{
				particle = new ParticleSquirter("clockEffect.particle");			
				particle.repeatable = true;
				particle.squirtAt(center.x, center.y);
			}
			else
			{
				particle.squirtAt(center.x, center.y);
			}

			particle.update(dt);
		}
        
    }
}

